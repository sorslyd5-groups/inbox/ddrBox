# meta

this page tracks links to inspirations for this project.

# frame
- https://fab.cba.mit.edu/classes/863.19/CBA/people/jack/week-06.html

# switches
- https://www.xda-developers.com/mechanical-keyboards-guide/
    - ![switch](https://static1.xdaimages.com/wordpress/wp-content/uploads/2021/06/Membrame-key-switch-exploded-view.jpg)
    - ![](https://www.xda-developers.com/files/2021/06/Mechanical-switch-exploded-view.jpg)
# capacitive sensors
- http://fab.cba.mit.edu/classes/863.22/CBA/people/Leandra/pages/week9.html
- https://academy.cba.mit.edu/classes/input_devices/step/hello.txrx.t1624.mp4
- https://www.hackster.io/gatoninja236/capacitive-touch-sensing-grid-f98144
 
# compliance
- https://all3dp.com/2/3d-printed-spring-tips-tricks/
- https://www.mcmaster.com/products/foam/resilient-polyurethane-foam-sheets-and-strips/
- https://www.amazon.com/Premium-Cosplay-Density-Projects-MEARCOOH/dp/B09GJQMFS4/ref=sr_1_5?crid=3OFC5A2ZSG914&keywords=foam&qid=1696206382&sprefix=fo%2Caps%2C233&sr=8-5&th=1


