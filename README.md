# meta

ddr box project for 99 fridays fa'23

theme is aLiEnS

# links

- [google photos](https://photos.app.goo.gl/3v2GtEnLh5MZUxwg7)
- [people](/docs/people.md)
- [inspo](/docs/inspo.md)

| | |
|-|-|
|![](/media/remote/hero/meme.jpg)|![](/media/remote/Screenshot_2023-10-02_140150.png)|

# body

## assembly

|w/ frame | w/o frame|
|-|-|
|![](/media/remote/Screenshot_2023-10-02_140201.png)|![](/media/remote/Screenshot_2023-10-02_140150.png)|

The assembly consists of 4 major subcomponents:

- floor, focus of input for players
- screen & projector, focus of output for players
- walls, mostly aesthetic
- frame, to keep everything together and rigid


there's a lot of room for additions, but depends on time quite a bit. I would assume we've already run out of time and work up towards a mvp!

## floor

the original idea for this; ddr but bigger than a 3x3 pad (caveat that there are [at least a few dance games](https://www.youtube.com/watch?v=4vAt-nMOC8Q) that have surpassed these limitations).

I thought it might be fun to go for 7x7, as that's the template for the Mourier font (which is the font that makes up part of the visual language of the media lab).

It might be even cooler if each of those 49 tiles themselves had a 7x7 led matrix, but this might introduce a lot of additional complexity.

- neopixels, 50x50 ws2812b rgb
    - 4020 rt angle rgb
- COB leds
    - single color (white)

![](/media/remote/Screenshot_2023-10-01_212710.png)

### single tile
|iso|side|
|-|-|
|![](/media/remote/Screenshot_2023-10-02_140351.png)|![](/media/remote/Screenshot_2023-10-01_212409.png)|

|explode|
|-|
|![](/media/remote/Screenshot_2023-10-02_140428.png)|

|switch1|switch2|
|-|-|
|![switch](https://static1.xdaimages.com/wordpress/wp-content/uploads/2021/06/Membrame-key-switch-exploded-view.jpg)|![](https://www.xda-developers.com/files/2021/06/Mechanical-switch-exploded-view.jpg)|

## screen & projector
|picoprojector|screen|
|-|-|
|Alan owns this [picoprojector](https://m.media-amazon.com/images/I/51Zdx-mLcML._AC_SL1000_.jpg) that gives us variable throw distance|screen can be a projection screen, cloth, vinyl, something|
|![](https://m.media-amazon.com/images/I/51Zdx-mLcML._AC_SL1000_.jpg)|![](/media/remote/screen.png)|

## walls

the walls can be additional interactive surfaces like the original idea, but that might be a lot of additional work. I would love to see it, but I'm treating the walls as aesthetic only right now.

- opportunity to vinyl cut roll
- laser etch acrylic
    - ![](/media/remote/acrylic.png)

## frame

considering co-opting HTMAA make something big for this.
cnc plywood w/ joints; creates a frame to attach projector, screen, walls, and house floor.

- Jack Forman's bunkbed
    - ![Jack Forman's bunkbed](https://fab.cba.mit.edu/classes/863.19/CBA/people/jack/Week5/images/glamour.jpeg)
